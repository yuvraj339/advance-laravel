{!! Form::open() !!}
<div class="form-group col-lg-6 col-md-6">
{!! Form::text('name', null, ['placeholder'=>'Your Name', 'class'=> 'form-control'] ); !!}<br/>
{!! Form::text('mobile', null, ['placeholder'=>'Your 10 Digit MobileNo.', 'class'=> 'form-control'] ); !!}<br/>
{!! Form::email('email', null, ['placeholder'=>'Your Email', 'class'=> 'form-control'] ); !!}<br/>
{!! Form::textarea('body', null, ['placeholder'=>'Your Query', 'size' => '30x10', 'class'=> 'form-control'] ); !!}<br/>
{!! Form::submit('Send Email', ['name'=>'btn_send', 'class'=>'form-control']) !!}<br/>
{!! Form::close() !!}
</div>