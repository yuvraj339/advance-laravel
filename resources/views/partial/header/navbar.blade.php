<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ url('home') }}" class="navbar-brand">Logo</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li <?=echoActiveClassIfRequestMatches("home")?>><a href="{{ url('home') }}"><i class="fa fa-home"></i> Home</a></li>
                <li <?=echoActiveClassIfRequestMatches("contact")?>><a href="{{ url('contact') }}"><i class="fa fa-phone-square"></i> Contact</a></li>
            </ul>

            {!! Form::open(['url' => '/homesearch', 'method' => 'get', 'class' => 'navbar-form navbar-right', 'role' => 'search']) !!}
            <div class="input-group">
                {!! Form::text('typeahead', null, ['id'=>'typeahead' , 'class' => 'typeahead tt-query', 'placeholder' => 'Type your Query', 'autocomplete'=> 'off', 'spellcheck'=>'false' ]) !!}
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><span class="fa fa-search"></span></button>
                </span>

            </div>
            {!! Form::close() !!}


        </div><!--/.nav-collapse -->

    </div>
</div>
<?php
function echoActiveClassIfRequestMatches($requestUri)
{ $current_file_name = basename($_SERVER['REQUEST_URI']);
    if ($current_file_name == $requestUri)
        echo 'class="active"';
}

?>
