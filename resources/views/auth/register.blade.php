@extends('app')
@section('content')


<div class="row col-md-12">
    <div class="col-md-5">
        <h1>Registration : New User</h1>
<hr>
        {!! Form::open() !!}
        {!! Form::text('name', null, ['placeholder'=>'Enter Your Name', 'class'=> 'form-control'] ); !!}<br/>
        {!! Form::text('email', null, ['placeholder'=>'Enter Your Email', 'class'=> 'form-control'] ); !!}<br/>
        {!! Form::password('password', ['placeholder'=>'Enter Your Password', 'class'=> 'form-control'] ); !!}<br/>
        {!! Form::password('password_confirmation', ['placeholder'=>'Enter Confirm Password', 'class'=> 'form-control'] ); !!}<br/>

        {!! Form::submit('Register', ['class'=> 'btn btn-default btn-lg']) !!} &nbsp;  
        <a href="login">{!! Form::button('Already have Account', ['class'=> 'btn btn-success btn-lg ']) !!}</a>
        {!! Form::close() !!}
    </div>
    
     <div class="col-md-4 pull-right">
        {!! Html::image('images/register.png', 'login') !!}
    </div>
  
</div>
{{--@include('errors.error_list')--}}

@stop

