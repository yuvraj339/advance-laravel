@extends('app')
@section('content')


<div class="row col-md-10 center-block">
    <div class="col-md-5">
        <h1>Login : User</h1>
<hr>
        {!! Form::open() !!}
        {!! Form::text('email', null, ['placeholder'=>'Enter Your Email', 'class'=> 'form-control'] ); !!}<br/>
        {!! Form::password('password', ['placeholder'=>'Enter Your Password', 'class'=> 'form-control'] ); !!}<br/>

        {!! Form::submit('Login', ['class'=> 'btn btn-default btn-lg']) !!} &nbsp;  
        <a href="register"> <button type="button" class="btn btn-danger btn-lg">Don't have Account</button></a>
        {!! Form::close() !!}
    </div>
    <div class="col-md-5">
        {!! Html::image('images/login7.jpg', 'login') !!}
        <!--<img src="../../../public/images/login7.jpg" alt=""/>-->
    </div>

</div>
{{--@include('errors.error_list')--}}

@stop

