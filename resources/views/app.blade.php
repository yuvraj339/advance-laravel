<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/font-awesome.min.css') !!}
    {!! Html::style('css/jquery-ui.css') !!}
    {!! Html::style('css/style.css') !!}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

</head>
<body>
<div class="container contheight">
    @include('partial.header.navbar')
    @yield('content')

</div>
</body>
@yield('footer')

{!! Html::script('js/jquery.min.js') !!}
{!! Html::script('js/bootstrap.min.js') !!}
{!! Html::script('js/jquery-ui.js') !!}
{!! Html::script('js/typeahead.min.js') !!}

{{--start script for head search box--}}
<script>
    $(document).ready(function(){
        $('input.typeahead').typeahead({
            name: 'typeahead',
            remote:'data?key=%QUERY',
            limit : 10
        });
    });
</script>
{{--End script for head search box--}}

</html>