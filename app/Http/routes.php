<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('pages.home');

});Route::get('home', function () {
    return view('pages.home');
});
Route::get('contact', function () {
    return view('pages.contact-us');
});


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('/homesearch', 'MainController@query');
Route::get('finder/{searchResult}', 'MainController@finderData');
Route::get('key', 'MainController@searchData');