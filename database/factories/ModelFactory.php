<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});


//// define(App\Modelname::
//$factory->define(App\ad::class, function ($faker) {
//    return [
//        'title' => $faker->sentence($nbWords = 4),
//        'user_id' => 1,
//        'body' => $faker->paragraph($nbSentences = 3),
//        'available_from' => Carbon\Carbon::now(),
//        'price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 10000),
//        'area'  => $faker->numberBetween($min = 1, $max = 8),
//        'address' =>$faker->word,
//    ];
//});